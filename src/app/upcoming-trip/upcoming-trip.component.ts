import { Component, OnInit } from '@angular/core';
import { DashboardServices } from '../services/dashboard-service';

@Component({
  selector: 'app-upcoming-trip',
  templateUrl: './upcoming-trip.component.html',
  styleUrls: ['./upcoming-trip.component.css'],
  providers: [DashboardServices]
})
export class UpcomingTripComponent implements OnInit {
  noTrip = false;
  tripDetails:any;
  tripsDetailPop:any;
  numOfPersons:any;
  
  constructor(private dashService: DashboardServices) { }

  ngOnInit() {
    this.dashService.getTrips('UPCOMING').subscribe((res) => {
      if(res && res['body']){ console.log('cancel trip',res);
        if(res['body'].tour == null){
          this.noTrip = true; 
        }else if(res['body'].tour.length==0){
          this.noTrip = true; 
        } else{
          this.tripDetails = res['body'].tour;console.log('upcomig',this.tripDetails);
          this.tripDetails.filter((res , i) =>{
            this.tripDetails[i].hideModal = true;
            if(res.tour_name.length>22){
              res.tour_name = res.tour_name.substr(0,20) + '…'; 
            }
          });
        }
      }
    });
  }
  getTourDetail(id , tripType , bId){
    this.tripDetails.filter((r , i) =>{
      if(r.id != id){
        r.hideModal = false;
      }else{
        r.hideModal = true;
      }
    });
    this.dashService.getTripDetail(id , tripType , bId).subscribe((res) => {
      if(res && res['body']){
        this.tripsDetailPop = res['body'].tripDetails; 
        this.numOfPersons = parseInt(this.tripsDetailPop.no_of_adults) + 
        parseInt(this.tripsDetailPop.no_of_children);
      }
    });
  }
  cancelTrip(id , uId){
    document.getElementById('close').click();
    this.dashService.cancelAfterPayment(id , uId).subscribe((res) =>{
      console.log(res);
    });
  }
}
