import { Component, OnInit } from '@angular/core';
import {CommonHeader} from '../commonHeader.service';
import { BlockingProxy } from 'blocking-proxy';

@Component({
  selector: 'app-guide-dashboard',
  templateUrl: './guide-dashboard.component.html',
  styleUrls: ['./guide-dashboard.component.css']
})
export class GuideDashboardComponent implements OnInit {

  constructor(private commonHeader: CommonHeader) { }

  ngOnInit() {
    this.commonHeader.applyHeaderClass(false);  
  }
}
