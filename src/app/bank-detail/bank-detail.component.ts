import {Component, OnInit} from '@angular/core';
import {CommonHeader} from '../commonHeader.service';
import {BankDetail} from '../services/bank-detail-service';
import {BankDetailModel} from './bank-detail-model';
import {ToastrService} from 'ngx-toastr';
import {IOption} from 'ng-select';
@Component({
  selector: 'app-bank-detail',
  templateUrl: './bank-detail.component.html',
  styleUrls: ['./bank-detail.component.css'],
  providers:[BankDetail]
})
export class BankDetailComponent implements OnInit {
  guideToken = localStorage.getItem('guide_id'); 
  bank = {anum:'', canum:'' , ifsc:'' , pan:'' , gst:'' , bId:''};
  selectedBankId;
  haveGst = false;
  bankSavedData = '';
  constructor(private commonHeader: CommonHeader , private bankDetail: BankDetail , 
  private toastr: ToastrService) { }
  myOptions: Array<IOption> = [];
  ngOnInit() {
    this.commonHeader.applyHeaderClass(false);
    this.bankDetail.getBankList().subscribe(res => {
      if(res["body"] && res["body"].bankList.length>0){
        for(let i=0 ; i<res["body"].bankList.length; i++){
          let a = {label:'', value:''}
          a["label"] = res["body"].bankList[i].name;
          a["value"] = res["body"].bankList[i].id;
          this.myOptions.push(a);
        }console.log(this.myOptions);
      }
    });
    this.getBankSavedData();
  }
  showError(message) {
    this.toastr.error(message);
  }
  onSelectedBank(event){
    console.log(event);
    this.selectedBankId = event;
  }
  matchAccountNumber(anum , canum){
    if(anum == canum){
      return true;
    }else{
      return false;
    }
  }
  gstCheck(val){
    if(val=="haveGst"){
      this.haveGst = true;
    }
    else{
      this.haveGst = false;
    }
  }
  bankDetailObj:any;
  onSubmit(formData){
    if(this.matchAccountNumber(formData.anum , formData.canum)){
      if(this.selectedBankId == 'Select' || this.selectedBankId==''){
        this.toastr.warning('Pls select bank');
      }else if(this.haveGst){
        if(formData.gst == ''){
          this.toastr.warning('Enter Gst Number');
          return false;
        }else {
          this.bankDetailObj = new BankDetailModel(this.guideToken , this.selectedBankId , formData.anum ,
            formData.ifsc , formData.pan , formData.gst);
            console.log(this.bankDetailObj);
        }
      }else{
        this.bankDetailObj = new BankDetailModel(this.guideToken , this.selectedBankId , formData.anum ,
        formData.ifsc , formData.pan , '');
        console.log(this.bankDetailObj);
      }
      this.bankDetail.saveBankDetail(this.bankDetailObj).then(res => {
        console.log(res);
        this.toastr.success('BANK DETAIL SAVED');
      });
    }else{
      this.showError("Account Number Doesn't match");
    }
  }
  getBankSavedData(){
    this.bankDetail.getBankDetail().subscribe(res =>{
      if(res["body"] && res["body"].details){console.log('bank detail',res["body"].details);
        this.bankSavedData = res["body"].details;
         this.bank.anum  = this.bankSavedData["account_number"];
         this.bank.canum = this.bankSavedData["account_number"];
         this.bank.ifsc  = this.bankSavedData["ifsc_code"];
         this.bank.pan   = this.bankSavedData["pan_card"];
         this.bank.gst   = this.bankSavedData["gst_number"];
         this.bank.bId   = this.bankSavedData["bank_id"];  
         this.selectedBankId  = this.bankSavedData["bank_id"];  
         if(this.bank.gst!=''){
           this.haveGst = true;
         }
      }
    });
  }
}
