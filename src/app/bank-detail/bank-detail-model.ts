export class BankDetailModel {
    guide_id:any;
    bank_id:any;
    account_number:any;
    ifsc_code:any;
    pan_card:any;
    gst_number:any;
    constructor(gId, bId , accNum , ifscCode , panCard , gstNumber){
        this.guide_id = gId;
        this.bank_id = bId;
        this.account_number = accNum;
        this.ifsc_code = ifscCode;
        this.pan_card = panCard;
        this.gst_number = gstNumber;
    }
}