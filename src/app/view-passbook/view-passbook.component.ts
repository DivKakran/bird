import { Component, OnInit } from '@angular/core';
import { Rating } from '../services/rating.service';

@Component({
  selector: 'app-view-passbook',
  templateUrl: './view-passbook.component.html',
  styleUrls: ['./view-passbook.component.css']
})
export class ViewPassbookComponent implements OnInit {
  passbookDetail:any;
  type = 'TODAY'
  constructor(private passBookService: Rating) { }

  ngOnInit() {
   this.getPassbookDetail('TODAY');
  }
  getPassbookDetail(type){
    this.type = type;
    this.passBookService.getPassbookDetail(type).subscribe((res) =>{
      if(res && res['body']){
        this.passbookDetail = res['body'];
        console.log('passbook detail',res['body']);
      }
    });
  }
}
