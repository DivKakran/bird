import {Injectable} from '@angular/core';
import {HttpClient , HttpHeaders , HttpRequest} from '@angular/common/http'
import {environment} from '../../environments/environment'
@Injectable()
export class Rating{
    ratingUrl = environment.baseUrl + "guide_tour/ratingEarning";
    passBookUrl = environment.baseUrl + "passbook/passbookDetails";
    guideId = localStorage.getItem('guide_id');
    constructor(private _http : HttpClient){}
    httpHeaders = new HttpHeaders({'Content-Type':'application/json'}).append('Authorization','Basic YWRtaW46MTIzNA==');
    getRating(){
        let obj = {guide_id : this.guideId};
        return this._http.request(new HttpRequest('POST' , this.ratingUrl , obj , 
        {headers: this.httpHeaders}));
    }
    getPassbookDetail(typ){
        let obj = {guide_id : this.guideId , type : typ};
        return this._http.request(new HttpRequest('POST' , this.passBookUrl , obj , 
        {headers: this.httpHeaders}));
    }
} 