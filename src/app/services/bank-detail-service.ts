import {Injectable} from '@angular/core';
import {HttpClient , HttpHeaders , HttpRequest} from '@angular/common/http';
import {environment} from '../../environments/environment';
@Injectable()
export class BankDetail {
    userToken = localStorage.getItem('guide_id')
    bankListUrl = environment.baseUrl+"guide_document/bank";
    bankDetailSaveUrl = environment.baseUrl+"guide_document/updateBankDetails";
    getBankSavedData = environment.baseUrl+"guide_document/bankDetails";
    httpHeaders = new HttpHeaders({'Content-Type':'application/json'}).append('Authorization', "Basic YWRtaW46MTIzNA");
    constructor(private _http:HttpClient){}
    getBankList(){
        let obj = {};
        return this._http.request(new HttpRequest('POST', this.bankListUrl , obj,
        {headers:this.httpHeaders}));
    }
    saveBankDetail(bankDetail){
        console.log('bank ob j',bankDetail);
        return this._http.request(new HttpRequest('POST' , this.bankDetailSaveUrl , bankDetail , 
        {headers: this.httpHeaders})).toPromise();
    }
    getBankDetail(){
        let obj = {guide_id:localStorage.getItem('guide_id')}
        return this._http.request(new HttpRequest('POST' , this.getBankSavedData , obj ,
        {headers:this.httpHeaders}));
    }
}