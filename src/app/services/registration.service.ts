import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient , HttpRequest , HttpHeaders} from '@angular/common/http';
@Injectable()
export class Registration{
    getRegistrationData = environment.baseUrl+'guide_home/home';
    saveRegistrationData = environment.baseUrl+'guide_login/register';
    guideDetailUrl = environment.baseUrl+'guide_login/guideDetails';
    updateGuideDetailUrl = environment.baseUrl+'guide_login/updateProfile';
    guideId = localStorage.getItem('guide_id');
    constructor(private _http:HttpClient){}
    httpHeaders = new HttpHeaders({'Authorization':'Basic YWRtaW46MTIzNA=='});
    getRegistrationPageDetail(){
        return this._http.request(new HttpRequest('GET', this.getRegistrationData ,{headers:this.httpHeaders}));
    }
    saveGuideDetail(guide_data){
        let httpHeaders = new HttpHeaders({'Authorization':'Basic YWRtaW46MTIzNA=='}).append('Content-Type','application/json');
        return this._http.request(new HttpRequest('POST' , this.saveRegistrationData ,
        JSON.stringify(guide_data) , {headers:httpHeaders}));
    }
    getGuideDetailPageLoad(){
        let guideId = {"guide_id":this.guideId};
        return this._http.request(new HttpRequest('POST',this.guideDetailUrl, guideId , {headers:this.httpHeaders})).toPromise();
    }
    updateGuideDetail(guide_data){
        return this._http.request(new HttpRequest('POST',this.updateGuideDetailUrl, guide_data , {headers:this.httpHeaders}));
    }
}