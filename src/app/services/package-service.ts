import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import { HttpClient , HttpHeaders , HttpRequest } from '@angular/common/http';
@Injectable()
export class PackageService {
    getPackageDeatilUrl = environment.baseUrl+"package/city";
    createPackageUrl = environment.baseUrl+"package/savePackage";
    getPackageUrl = environment.baseUrl+"package/packagelistingWeb";
    deletePackageUrl = environment.baseUrl+"package/deletePackage";
    guideId = localStorage.getItem('guide_id');
    httpHeaders = new HttpHeaders({'Content-Type':'application/json'}).append('Authorization','Basic YWRtaW46MTIzNA==');
    constructor(private _http: HttpClient) {}
    getPackageDetail(){
        let obj ={}
        return this._http.request(new HttpRequest('POST',this.getPackageDeatilUrl,obj,
        {headers:this.httpHeaders}));
    }
    createPackage(pack){
        return this._http.request(new HttpRequest('POST',this.createPackageUrl,pack,
        {headers:this.httpHeaders})).toPromise();
    }
    getAllCreatedPackageOnPageLoad(i){
        let obj = {guide_id:this.guideId,page:'4',offset:i};
        return this._http.request(new HttpRequest('POST',this.getPackageUrl , obj ,
        {headers:this.httpHeaders}));
    }
    deletePackage(pId){
        let obj = {guide_id:this.guideId,package_id:pId};
        return this._http.request(new HttpRequest('POST',this.deletePackageUrl , obj ,
        {headers:this.httpHeaders}));
    }
}