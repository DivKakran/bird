import {Injectable} from '@angular/core';
import {HttpClient , HttpHeaders , HttpRequest} from '@angular/common/http';
import {environment} from '../../environments/environment'
@Injectable()
export class DashboardServices{
    userToken = localStorage.getItem('guide_id');
    tripUrl   = environment.baseUrl+'guide_tour/myTour';
    tourDetailUrl = environment.baseUrl+'guide_tour/myTourDetails';
    rejectRequestUrl = environment.baseUrl+'guide_tour/rejectRequest';
    acceptRequestUrl = environment.baseUrl+'guide_tour/acceptRequest';
    cancelTripUrl = environment.baseUrl+'guide_search/pendingTripCancel';
    cancelTripAfterPaymentUrl = environment.baseUrl+'guide_tour/upcomingTourCancel';
    constructor(private _http: HttpClient){}
    httpHeaders = new HttpHeaders({'Content-Type':'application/json'}).append('Authorization', "Basic YWRtaW46MTIzNA");
    getTrips(type){
        let obj = {trip_type:type , guide_id:this.userToken } ;
        return this._http.request(new HttpRequest('POST', this.tripUrl , obj ,{
            headers:this.httpHeaders
        }));
    }
    getTripDetail(id , tripType , bId){
        let obj = {guide_id:this.userToken , id:id , trip_type:tripType , booking_id:bId}; console.log('view Detail ',obj)
        return this._http.request(new HttpRequest('POST' , this.tourDetailUrl , obj ,{
            headers:this.httpHeaders
        }));
    }
    acceptRequest(uId , id){
        let obj = {user_id: uId , id: id , guide_id: this.userToken};
        return this._http.request(new HttpRequest('POST' , this.acceptRequestUrl , obj , {
            headers:this.httpHeaders
        }));
    }
    rejectRequest(uId , id){
        let obj = {user_id: uId , id: id , guide_id: this.userToken};
        return this._http.request(new HttpRequest('POST' , this.rejectRequestUrl , obj , {
            headers:this.httpHeaders
        }));
    }
    cancelTrip(uId , id){
        let obj = {user_id: uId , id: id , guide_id: this.userToken};
        return this._http.request(new HttpRequest('POST' , this.cancelTripUrl , obj , {
            headers:this.httpHeaders
        }));
    }
    cancelAfterPayment(id , uId){
        let obj = {user_id: uId , id: id , guide_id: this.userToken};
        return this._http.request(new HttpRequest('POST' , this.cancelTripAfterPaymentUrl , obj , {
            headers:this.httpHeaders
        }));
    }
} 