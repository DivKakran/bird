import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import { HttpClient , HttpHeaders , HttpRequest } from '@angular/common/http';
@Injectable()
export class GetOtp {
    getOtpUrl = environment.baseUrl+'guide_login/requestOTP';
    verfiyOtp = environment.baseUrl+'guide_login/verifyOTP';
    constructor(private _http: HttpClient) {}
    httpHeaders = new HttpHeaders({'Content-Type':'application/json'}).append('Authorization','Basic YWRtaW46MTIzNA==');
    gettingOtp(otpModel){
        return this._http.request(new HttpRequest('POST', this.getOtpUrl ,JSON.stringify(otpModel),{headers:this.httpHeaders}));
    }
    verifyOtp(otp){
        return this._http.request(new HttpRequest('POST', this.verfiyOtp ,JSON.stringify(otp),{headers:this.httpHeaders}))
    }
}