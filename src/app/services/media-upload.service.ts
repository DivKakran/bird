import {Injectable} from '@angular/core';
import { HttpClient , HttpHeaders , HttpRequest } from '@angular/common/http';
import {environment} from '../../environments/environment';
@Injectable()
export class UploadMedia{
    constructor(private _http: HttpClient) {}
    httpHeaders = new HttpHeaders({'Authorization':'Basic YWRtaW46MTIzNA=='});
    profilePicUpload = environment.baseUrl+"guide_login/updateprofileimage";
    galleryUpload = environment.baseUrl+"guide_login/updateGalleryImage";
    getGuideGalleryUrl = environment.baseUrl+"guide_login/guidegallery";
    updateGalleryImageUrl = environment.baseUrl+"guide_login/editGalleryImage";
    youtubeVideoUrl = environment.baseUrl+"guide_login/updatevideo";
    uploadProfilePicture(guide_id , pic_url){
        let formData = new FormData();
        formData.append("guide_id",guide_id);
        formData.append("imagefile",pic_url); console.log('pf',formData);
        return this._http.request(new HttpRequest('POST', this.profilePicUpload , formData , 
        {headers:this.httpHeaders}));
    }
    uploadGuideGallery(guide_id, pic_url){
        let formData = new FormData();
        formData.append("guide_id",guide_id);
        formData.append("imagefile",pic_url);
        return this._http.request(new HttpRequest('POST', this.galleryUpload , formData , 
        {headers:this.httpHeaders}));
    }
    getGuideGallery(guide_id){
        let guideId = {"guide_id":guide_id};
        return this._http.request(new HttpRequest('POST',this.getGuideGalleryUrl , guideId,
        {headers: this.httpHeaders}));
    }
    updateGalleryImage(guideId , image , imgId){ console.log(image);
        let formData = new FormData();
        formData.append('guide_id',guideId);
        formData.append("imagefile",image);
        formData.append("id",imgId);
        return this._http.request(new HttpRequest('POST',this.updateGalleryImageUrl , formData,
        {headers: this.httpHeaders}))
    }
    uploadYouTubeVideo(guideId , videoLink){
        let video = {"guide_id":guideId , "video_link":videoLink};
        return this._http.request(new HttpRequest('POST',this.youtubeVideoUrl ,video ,
        {headers:this.httpHeaders}));
    }    
}