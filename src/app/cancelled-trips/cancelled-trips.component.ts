import { Component, OnInit } from '@angular/core';
import {DashboardServices} from '../services/dashboard-service';

@Component({
  selector: 'app-cancelled-trips',
  templateUrl: './cancelled-trips.component.html',
  styleUrls: ['./cancelled-trips.component.css'],
  providers: [DashboardServices]
})
export class CancelledTripsComponent implements OnInit {
  noTrip = false;
  tripDetails:any;
  tripsDetailPop:any;
  numOfPersons:any;
  setOffset = 0;
  constructor(private dashService: DashboardServices) { }

  ngOnInit() {
   this.getTripListing(this.setOffset);
  }
  getTripListing(offset){
    this.dashService.getTrips('CANCELLED').subscribe((res) => {
      if(res && res['body']){
        if(res['body'].tour == null){
          this.noTrip = true; 
        }else if(res['body'].tour.length==0){
          this.noTrip = true; 
        } else{
          this.tripDetails = res['body'].tour; console.log('trip details',res['body']);
          this.tripDetails.filter((res , i) =>{
            this.tripDetails[i].hideModal = true;
            if(res.tour_name.length>22){
              res.tour_name = res.tour_name.substr(0,20) + '…'; 
            }
          });
        }
      }
    });
  }
  getTourDetail(id , tripType , bId){ 
    this.tripDetails.filter((r , i) =>{
      if(r.id != id){
        r.hideModal = false;
      }else{
        r.hideModal = true;
      }
    });
    this.dashService.getTripDetail(id , tripType , bId).subscribe((res) => {
      if(res && res['body']){
        this.tripsDetailPop = res['body'].tripDetails; 
        this.numOfPersons = parseInt(this.tripsDetailPop.no_of_adults) + 
        parseInt(this.tripsDetailPop.no_of_children);
      }
    });
  }
  checkStatusDetail(request_status){
   if(request_status == "6"){
      return "*Request Cancelled by Tourist";
   }else if(request_status == "3"){
      return "*Request has been Expired";
   }else if(request_status == "2"){
      return "*Request Rejected by Guide";
   }
  }
  getCancelledTripListing(type){
    if(type == 'next'){
      this.getTripListing(this.setOffset+1);
      this.setOffset+=1;
    }else if(type == 'pre'){
      this.getTripListing(this.setOffset-1);
      this.setOffset-=1;
    }
  }
}
  
