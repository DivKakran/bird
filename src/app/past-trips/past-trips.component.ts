import { Component, OnInit } from '@angular/core';
import { DashboardServices } from '../services/dashboard-service';

@Component({
  selector: 'app-past-trips',
  templateUrl: './past-trips.component.html',
  styleUrls: ['./past-trips.component.css'],
  providers: [DashboardServices]
})
export class PastTripsComponent implements OnInit {
  noTrip = false;
  tripDetails: any;
  tripsDetailPop:any;
  numOfPersons:any;
  constructor(private dashService: DashboardServices) { }

  ngOnInit() {
    this.dashService.getTrips('PAST').subscribe((res) => {
      if(res && res['body']){console.log('past trips',res);
        if(res['body'].tour == null){
          this.noTrip = true; 
        }else if(res['body'].tour.length==0){
          this.noTrip = true; 
        } else{
          this.tripDetails = res['body'].tour;
          this.tripDetails.filter((res , i) =>{
            this.tripDetails[i].hideModal = true;
            if(res.tour_name.length>22){
              res.tour_name = res.tour_name.substr(0,20) + '…'; 
            }
          });
        }
      }
    });
  }
  getTourDetail(id , tripType , bId){ 
    this.tripDetails.filter((r , i) =>{
      if(r.id != id){
        r.hideModal = false;
      }else{
        r.hideModal = true;
      }
    });
    this.dashService.getTripDetail(id , tripType , bId).subscribe((res) => {
      if(res && res['body']){
        this.tripsDetailPop = res['body'].tripDetails; 
        this.numOfPersons = parseInt(this.tripsDetailPop.no_of_adults) + 
        parseInt(this.tripsDetailPop.no_of_children);
      }
    });
  }

}
