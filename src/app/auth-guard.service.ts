import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router){}
  id='';
  canActivate() {
    this.id = localStorage.getItem('guide_id');
    if(this.id==null){
        this.router.navigate(['']);
        return false;
    }else{
        return true;
    }
  }
}