export class RegistrationModel{
    guide_id:any;
    name:any;
    email:any;
    experience_years:any;
    gender:any;
    device_type:any;
    experience_months:any;
    about_me:any;
    dob:any;
    secondary_contact:any;
    uuid_value:any;
    token_key:any;
    language_ids=[];
    address:any;
    constructor(guide_id,name,email,experience,gender,device_type,experience_month,about_me,dob,
        snumber,uuid_value,languages,address){
        this.guide_id = guide_id;
        this.name = name;
        this.email = email;
        this.experience_years= experience;
        this.gender = gender;
        this.device_type = device_type;
        this.experience_months = experience_month;
        this.about_me = about_me;
        this.dob = dob;
        this.secondary_contact= snumber;
        this.uuid_value = uuid_value;
        this.language_ids = languages;
        this.address = address;
    }
}
export class Address{
    state_id:any;
    city_id:any;
    address:any;
    pincode:any;
    constructor(stateId,cityId,address,pincode){
        this.state_id = stateId;
        this.city_id  = cityId;
        this.address  = address;
        this.pincode  = pincode;
    }
}