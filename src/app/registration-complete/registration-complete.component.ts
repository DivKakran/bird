import { Component, OnInit , AfterViewInit} from '@angular/core';
import {CommonHeader} from '../commonHeader.service';
import {UploadMedia} from '../services/media-upload.service';
import {Registration} from '../services/registration.service';
import {RegistrationModel,Address} from './registration-model';
import {DomSanitizer} from '@angular/platform-browser'
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration-complete',
  templateUrl: './registration-complete.component.html',
  styleUrls: ['./registration-complete.component.css'],
  providers:[]
})
export class RegistrationCompleteComponent implements OnInit{
  formData={pinCode:'',name:'',email:'',pnumber:'',snumber:'',about:'',dob:new Date(),address:''};
  stateList=["Up","Haryana","Rjasthan"];
  uploadImageGallery=[{"id":"0","img":"", "default":"../../assets/images/gallery-uploadBox.png"},
                      {"id":"1","img":"", "default":"../../assets/images/gallery-uploadBox2.png"},
                      {"id":"2","img":"", "default":"../../assets/images/gallery-uploadBox3.png"},
                      {"id":"3","img":"", "default":"../../assets/images/gallery-uploadBox4.png"}];
  cityList=[];
  languages=[];
  dropdownSettings = {};
  selectedLanguages=[];
  // guide selected 
  guideSelectedLangugaes=[];
  guideSelectedState:any='';
  guideSelectedCity:any='';
  guideGender:any="Male";
  guideExpInYear:any = "0";
  guideExpInMonth:any = "0";
  uploadDocument = false;
  sGuideImage = [];
  i=0;
  updatedVideoLink;
  safeUrl;
  guideDetail='';
  guideFilledState='';
  guideFilledCity=';'

  url = localStorage.getItem('pic');
  docApproved = localStorage.getItem("docApproved");
  guideFilledLanguagesOnPageLoad ;
  updateButtonActive=false;

  
  // config = {max:'11-03-2010 10:20:50' , format:"DD-MM-YYYY" , openOnFocus: true , firstDayOfWeek:'we'}

  constructor(private router: Router,private commonHeader: CommonHeader , private mediaUpload:UploadMedia ,
  private registration:Registration , private toastr: ToastrService , private sanitizer: DomSanitizer) {
    
   }
  guide_id = localStorage.getItem('guide_id');

  showGuideDetailsOnPageLoad(detail){
    console.log('detail' , new Date(detail.dob * 1000));
    if(detail){
      this.guideFilledState = detail.address.state_id;
      this.guideFilledCity = detail.address.city_id;

      this.stateList.filter((res , i) =>{
        if(res["id"] == this.guideFilledState){
          this.guideSelectedState = res["id"];
          for(let state of this.stateList){
            if(state["id"] == res["id"]){
              this.cityList = state["cityList"];
            }
          }
        }
      });
      this.cityList.filter((res,i) =>{
        if(res["id"] == this.guideFilledCity){
          this.guideSelectedCity = res["id"];
        }
      });

      this.guideFilledLanguagesOnPageLoad = detail.language_ids;  
      if(this.guideFilledLanguagesOnPageLoad.length>0){ 
        this.languages.filter((res,i) =>{
          this.guideFilledLanguagesOnPageLoad.filter((r , j) =>{
            if(res.id == r){
              let obj = {id:r , name:res.name};
              this.selectedLanguages.push(obj); 
              this.guideSelectedLangugaes.push(r);
            }
          });
        });
      }

      this.formData.name = detail.name;
      this.formData.email = detail.email;
      this.formData.pnumber = detail.primary_contact;
      this.formData.snumber = detail.secondary_contact;
      this.formData.about = detail.about_me;
      this.formData.dob = new Date(detail.dob * 1000);
      this.formData.address = detail.address.address;
      this.formData.pinCode = detail.address.pincode;
      this.guideExpInYear = detail.experience_years;
      this.guideExpInMonth = detail.experience_months;
      this.guideGender = detail.gender; 
    }
  }
  ngOnInit() { 
    if(this.router.url == '/registration/docs'){
      this.uploadDocument = true;
    } else if(this.router.url == '/registration/create-package'){ 
      document.getElementById('places').click();
    } else if(this.router.url == '/registration/bank'){
      document.getElementById('bank').click();
    } else if(this.router.url == '/registration/rating-and-earning'){
      document.getElementById('rate').click();
    }else if(this.router.url == '/registration/my-profile'){
     this.uploadDocument = false;
    }else if(this.router.url == '/registration/my-places'){
      document.getElementById('places').click();
     }
    this.updatedVideoLink = 'https://www.youtube.com/embed/'+localStorage.getItem('video');
    this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.updatedVideoLink);
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 4,
      allowSearchFilter: true
    };
    this.commonHeader.applyHeaderClass(false);
    this.registration.getRegistrationPageDetail().subscribe(res=> {
      if(res && res["body"]){
        this.stateList = res["body"].stateList;
        this.languages = res["body"].languageList;
      }
    })
    this.mediaUpload.getGuideGallery(this.guide_id).subscribe(res => { 
      if(res && res["body"] && res["body"].galleryList.length>0){ 
      for(let i = 0; i< 4; i++){
          this.uploadImageGallery[i].img = res["body"].galleryList[i].image_url;
          this.uploadImageGallery[i].id = res["body"].galleryList[i].id;
        }
      }
    });
    this.registration.getGuideDetailPageLoad().then(res => { 
      console.log('guide details', res);
      if(res && res["body"] && res["body"].guideDetails && res["body"].guideDetails.name!=null){ 
        localStorage.setItem('dataFilled', 'true');
        this.guideDetail = res["body"].guideDetails; console.log('guide details', this.guideDetail);
        this.showGuideDetailsOnPageLoad(this.guideDetail);
        this.updateButtonActive = true;
      }else{
        localStorage.setItem('dataFilled', 'false');
      }
    });
  }
  fileToUpload;
  allowedExtensions;
  fileType;

  readUrl(event: any , fileUrl) { alert();
    this.gallery = false;
    this.imageChangedEvent = event;
    document.getElementById('openModalButtonC').click();
    //  this.fileUrl = fileUrl;
    // console.log('event value',event);
    // if (event.target.files && event.target.files[0]) {
    //   this.fileToUpload = event.target.files[0];
    //   console.log('profile upload ',this.fileToUpload);
    //   this.allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    //   const reader = new FileReader();
    //   reader.onload = (event:any) => {
    //   this.url = event.target.result;
    //   if (this.allowedExtensions.exec(fileUrl)) { 
    //     this.fileType = 'video';
    //     return false;
    //   } else {
    //     this.mediaUpload.uploadProfilePicture(this.guide_id , this.fileToUpload).subscribe(res => {
    //     console.log('patb',event);
    //     console.log('current',this.fileToUpload);
    //     if(res && res["body"]){
    //       localStorage.setItem('pic',res["body"].image_url);
    //       this.commonHeader.updateProfilePicture(res["body"].image_url);
    //     }
    //    });
    //     this.fileType = 'image';
    //   }
    //   };
    //   reader.readAsDataURL(event.target.files[0]);
    // }
  }

  selectedState(event){
    this.guideSelectedState = event;
    for(let state of this.stateList){
      if(state["id"] == event){
        this.cityList = state["cityList"];
      }
    }
  }
  selectdeCity(event){ 
    this.guideSelectedCity = event;
  }
  onLanguageSelect(event){
      this.guideSelectedLangugaes.push(event.id);
    }
  onLanguageDeSelect(event){
    this.guideSelectedLangugaes.filter((res ,i) => {
      if(res = event.id){
        this.guideSelectedLangugaes.splice(i,1);
      }
    });
    console.log('sdadasdasd',this.guideSelectedLangugaes);
  }
  getGender(event){
    this.guideGender = event;
  }
  guideExperienceYear(event){ 
    this.guideExpInYear = event;
  }
  guideExperienceMonth(event){
    this.guideExpInMonth = event;
  }
  showError(message) {
    this.toastr.error(message);
  }
  showWarnig(message){
    this.toastr.info(message);
  }
  checkStateSelected(){
    if(this.guideSelectedState=='' || this.guideSelectedState=='Select'){
      this.showWarnig('Select State First');
    }
  }
  checkGuideAge(data){
    var dob = new Date(data.dob);
    var currentDate = new Date();
    var guideEnteredDate = dob.getFullYear();
    var crt = currentDate.getFullYear(); 
    if(data.dob==null){
      return false;
    }
    if((crt-guideEnteredDate)>18){
      return true;
    }
    else{
      return false;
    }
  }
  submitRegistartionInfo(data){
    // this.commonHeader.activeLinks('true');
    // localStorage.setItem('dataFilled','true');
    if(this.guideSelectedCity=='' || this.guideSelectedState == '' || 
    this.guideSelectedCity=='Select' || this.guideSelectedState=='Select' || this.guideSelectedLangugaes.length==0){

      if(this.guideSelectedState=='' || this.guideSelectedState=='Select'){
        this.showError('Please Select State');
      }
      else if(this.guideSelectedCity=='' || this.guideSelectedCity=='Select'){
        this.showError('Please Select City');
      }
      else if(this.guideSelectedLangugaes.length == 0){
        this.showError('Select language');
      }
    }
    else{
      if(this.checkGuideAge(data)){ 
        let address = new Address(this.guideSelectedState,this.guideSelectedCity,
          data.address,data.pincode);
        if(address != null){ 
          let registrationData = new RegistrationModel(this.guide_id,data.name,data.email,
          this.guideExpInYear,this.guideGender,"WEB",this.guideExpInMonth,data.about_me,
          Math.round(new Date(data.dob).getTime()/1000),
          data.snumber,'122',this.guideSelectedLangugaes,address);

          if(!this.updateButtonActive){
            this.registration.saveGuideDetail(registrationData).subscribe(res =>{
            if(res && res["body"]){ 
              if(res["body"].status == 0){
                this.toastr.error(res["body"].message);
              }else{
                this.uploadDocument = true;
                this.commonHeader.chnageHeaderAfterLogin(res["body"].guideDetails);
              }
            }
          });
          }else{
            this.registration.updateGuideDetail(registrationData).subscribe(res =>{
              console.log(registrationData);
              console.log("address " , address);
              if(res && res["body"]){ 
                if(res["body"].status == 0){
                  this.toastr.error(res["body"].status);
                }else{
                  this.uploadDocument = true;
                  this.commonHeader.chnageHeaderAfterLogin(res["body"].guideDetails);
                  console.log('updated response', res["body"]);
                  this.toastr.success('Profile Updated Successfully');
                }
              }
            });
          }
        }
      }else{
        this.showError('Age should be above 18');
      }
    }
  }
  uploadGalleyIndexForLocal:any;
  updateGalleryImageId:any;
  gallery = false; 
  uploadGallery(event , fileUrl , imageId){
      this.gallery = true;
      this.uploadGalleyIndexForLocal = fileUrl;
      this.updateGalleryImageId = imageId;
      this.imageChangedEvent = event;
      document.getElementById('openModalButtonC').click();
    // if (event.target.files && event.target.files[0]) {
    //   this.fileToUpload = event.target.files[0];
    //   this.allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    //   const reader = new FileReader();
    //   reader.onload = (event:any) => {
    //   this.url = event.target.result;
    //   if (this.allowedExtensions.exec(fileUrl)) { 
    //     this.fileType = 'video';
    //     return false;
    //   } else {
    //     if(this.uploadImageGallery[fileUrl].img!=''){
    //       this.uploadImageGallery[fileUrl].img = this.url;
    //       this.mediaUpload.updateGalleryImage(this.guide_id ,this.fileToUpload , imageId).subscribe(res => {
    //         if(res && res["body"]){
    //         //  checking functionality
    //         }
    //       });
    //     }else{ 
    //       this.mediaUpload.uploadGuideGallery(this.guide_id , this.fileToUpload).subscribe(res => {
    //         if(res && res["body"]){
    //           this.uploadImageGallery[fileUrl].img = res["body"].image_url;
    //         }
    //       });
    //     }
    //     this.fileType = 'image';
    //   }
    //   };
    //   reader.readAsDataURL(event.target.files[0]);
    // }
  }
    uploadVideo(link){
      this.mediaUpload.uploadYouTubeVideo(this.guide_id , link).subscribe(res => {
        if(res && res["body"]){
          if(res["body"].status==1){
            this.updatedVideoLink = 'https://www.youtube.com/embed/'+res["body"].video_link_web;
            this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.updatedVideoLink);
            localStorage.setItem('video',this.safeUrl);
          }else{
            this.showError('Not a valid URL');
          }
        }
      });
    }
    changeUrl(url){
      this.router.navigate(['registration/'+url]);
    }
    onSelectAll(event){
      event.filter((res) => {
        this.guideSelectedLangugaes.push(res.id);
      });//console.log(this.guideSelectedLangugaes);
    }
    onDeSelectAll(event){
      this.guideSelectedLangugaes = []; 
      console.log(this.guideSelectedLangugaes);
    }
    // image cropper set
    imageChangedEvent: any = '';
    croppedImage: any = '';

    // fileChangeEvent(event: any): void {
    //   this.imageChangedEvent = event;
    //   document.getElementById('openModalButton').click();
    // }
    urltoFile(url, filename, mimeType){
      return (fetch(url)
        .then(function(res){
          return res.arrayBuffer();
        })
        .then(function(buf){
          return new File([buf], filename, {type:mimeType});
        })
      );
  }
  uploadFile:any;
  setImageOnCropped:any;
  uploadAndCropProfile(){
    this.mediaUpload.uploadProfilePicture(this.guide_id , this.uploadFile).subscribe(res => {
      if(res && res["body"]){
        this.url = this.setImageOnCropped;//for showing on upload
        localStorage.setItem('pic',res['body']['image_url']);
        this.commonHeader.updateProfilePicture(res["body"].image_url);
        console.log('sdddddddddddd',res);
      }
    });
  }
  uploadAndCropGallery(){
    if(this.uploadImageGallery[this.uploadGalleyIndexForLocal].img!=''){
      this.uploadImageGallery[this.uploadGalleyIndexForLocal].img = this.url;
      this.mediaUpload.updateGalleryImage(this.guide_id ,this.uploadFile , this.updateGalleryImageId)
      .subscribe(res => {
        if(res && res["body"]){
          this.uploadImageGallery[this.uploadGalleyIndexForLocal].img = res["body"].image_url;
        }
      });
    }else{ 
      this.mediaUpload.uploadGuideGallery(this.guide_id , this.uploadFile).subscribe(res => {
        if(res && res["body"]){
          this.uploadImageGallery[this.uploadGalleyIndexForLocal].img = res["body"].image_url;
        }
      });
    }
  }
  imageCropped(image: string) {
    this.croppedImage = image;
    this.setImageOnCropped = image;
    this.urltoFile(image, '75+points.png', 'image/png')
      .then((file) =>  {
        this.uploadFile = file
      });
    }
    imageLoaded(image) {
      // console.log(image.target.files[0]);
    }
    loadImageFailed() {
        // show message
    }
  }

