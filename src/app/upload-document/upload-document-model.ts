export class UploadDocument {
    doc_id:any;
    doc_type:any;
    guide_id:any;
    upload_type:any;
    view_type:any;
    image:any;
    constructor(docId, docType, guideId, uploadType, viewType, image){
        this.doc_id = docId;
        this.doc_type = docType;
        this.guide_id = guideId;
        this.upload_type = uploadType;
        this.view_type = viewType;
        this.image = image;
    }
}