import {HttpClient , HttpHeaders, HttpRequest} from '@angular/common/http'
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
@Injectable()
export class GetDocumentList {
    getDocmentList = environment.baseUrl+'guide_document/documenttype';
    uploadDocumentUrl = environment.baseUrl+'guide_document/uploaddoc';
    httpHeader = new HttpHeaders({'Authorization':'Basic YWRtaW46MTIzNA=='}).append('Content-Type','application/json');
    httpHeader2 = new HttpHeaders({'Authorization':'Basic YWRtaW46MTIzNA=='});
    constructor(private _http: HttpClient){}
    getDocumentList(userId){
        let guideId = {"guide_id":userId};
        return this._http.request(new HttpRequest('POST',this.getDocmentList , JSON.stringify(guideId), 
        {headers: this.httpHeader})).toPromise();
    }
    uploadDocument(imageData){
         return this._http.request(new HttpRequest('POST',this.uploadDocumentUrl,
         imageData , {headers: this.httpHeader2}));
    }
}