import { Component, OnInit} from '@angular/core';
import {CommonHeader} from '../commonHeader.service';
import {GetDocumentList} from './get-document-list.service';
import {UploadDocument} from './upload-document-model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-upload-document',
  templateUrl: './upload-document.component.html',
  styleUrls: ['./upload-document.component.css']
})
export class UploadDocumentComponent implements OnInit{
  active1 = false;
  active2 = false;
  active3 = false;
  imageDocId='';
  userId = localStorage.getItem('guide_id');
  selectRow;
  documentList:any;
  fileToUpload;
  imageUrl;
  allowedExtensions;
  fileType;
  sydOfImage;
  docType:any;
  uploading = false;
  activeProceedButton = true;
  
  guideId = localStorage.getItem('guide_id');
  constructor(private commonHeader: CommonHeader , private getDocs: GetDocumentList , private route: Router) { 
    this.commonHeader.applyHeaderClass(false);
  }

  ngOnInit() {
    this.getDocs.getDocumentList(this.userId).then(res => {
      if(res && res["body"]){ console.log('document list',res["body"]);
        this.documentList = res["body"].document_type;
        setTimeout(function(){ document.getElementById("fo").click(); }, 50);
        res["body"].document_type.filter((res,i) =>{
          if(res.name == "Address Proof"){
            console.log('ada',res);
            res.doc_status.filter((r,i)=>{
              if(r.view_type == "FRONT" && r.doc_url!=''){
                this.imagePreview[0].urlF = r.doc_url;
                this.imagePreview[0].type = r.doc_type_web;
                this.imagePreview[0].uploadedF = r.doc_uploaded;
                this.imagePreview[0].approvedF = r.doc_approved;
                r.uploading = true;
              }else{
                this.imagePreview[0].urlB = r.doc_url;
                this.imagePreview[0].type = r.doc_type_web;
                this.imagePreview[0].uploadedB = r.doc_uploaded;
                this.imagePreview[0].approvedB = r.doc_approved;
                r.uploading = true;
              }
            })
          }
          else if(res.name == "Identity Proof"){
            res.doc_status.filter((res,i)=>{
              if(res.view_type=="FRONT"){
                this.imagePreview[1].urlF = res.doc_url;
                this.imagePreview[1].type = res.doc_type_web; 
                this.imagePreview[1].uploadedF = res.doc_uploaded;
                this.imagePreview[1].approvedF = res.doc_approved;
              }else{
                this.imagePreview[1].urlB = res.doc_url;
                this.imagePreview[1].type = res.doc_type_web;
                this.imagePreview[1].uploadedB = res.doc_uploaded;
                this.imagePreview[1].approvedB = res.doc_approved;
              }
            });
          }
          else if(res.name == "Guide Licence"){
            res.doc_status.filter((res,i)=>{
              if(res.view_type == "FRONT"){
                this.imagePreview[2].urlF = res.doc_url;
                this.imagePreview[2].type = res.doc_type_web;
                this.imagePreview[2].uploadedF = res.doc_uploaded;
                this.imagePreview[2].approvedF = res.doc_approved;
              }else{
                this.imagePreview[2].urlB = res.doc_url;
                this.imagePreview[2].type = res.doc_type_web;
                this.imagePreview[2].uploadedB = res.doc_uploaded;
                this.imagePreview[2].approvedB = res.doc_approved;
              }
            }); console.log('data loaded of image',this.documentList);
          }
        });
      }
    });
  }
  selectActive;
  setImageDocTypeIfImageNotSet:any;
  selectedRow(index , docId , type ){
    this.setImageDocTypeIfImageNotSet = index;
    this.imageDocId = docId;
    if(this.selectRow == index){
      this.selectActive = 10;
    }else if(this.selectRow != index){
      this.selectActive = index;
    }
    this.selectRow  = index;
    if(this.imagePreview[index].uploadedF=='0' || this.imagePreview[index].uploadedB=='0' ){
      this.imageDocId = docId;
      this.imagePreview[index].type = type;
    }
  }
  
  checkDocType(type){
    this.docType = type;
    this.imagePreview[this.setImageDocTypeIfImageNotSet].type = type;
    console.log(this.docType);
  }

  imagePreview = [ {"doc_id":"" , "urlF":"" , "urlB":"" , "type":"" , "uploadedF":'' ,"uploadedB":'' ,"uploadingF":false,"uploadingB":false ,"approvedF":'',"approvedB":''} , 
                   {"doc_id":"" , "urlF":"" , "urlB":"" , "type":"" , "uploadedF":'' ,"uploadedB":'' ,"uploadingF":false,"uploadingB":false ,"approvedF":'',"approvedB":''} , 
                   {"doc_id":"" , "urlF":"" , "urlB":"" , "type":"" , "uploadedF":'' ,"uploadedB":'' ,"uploadingF":false,"uploadingB":false ,"approvedF":'',"approvedB":''}];

  readImageUrl(event , fileUrl ){ 
    this.activeProceedButton = false;
    this.imageChangedEvent = event;
    this.sydOfImage = 'FRONT';
    if (event.target.files && event.target.files[0]) {
      this.fileToUpload = event.target.files[0];
      console.log("upload image",this.fileToUpload);
      this.allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
      const reader = new FileReader();
      reader.onload = (event:any) => {
        if(this.imageDocId == '1'){
          this.imagePreview[0].urlF = event.target.result;
          // this.imagePreview[0].type = this.docType;
          this.imagePreview[0].uploadingF = true;
          this.imagePreview[0].approvedF = '';
          this.imagePreview[0].uploadedF = '1';
        }
        else if(this.imageDocId == '2'){
          this.imagePreview[1].urlF = event.target.result;
          // this.imagePreview[1].type = this.docType;
          this.imagePreview[1].uploadingF = true;
          this.imagePreview[1].approvedF = '';
          this.imagePreview[1].uploadedF = '1';
        }
        else if(this.imageDocId == '3'){
          this.imagePreview[2].urlF = event.target.result;
          // this.imagePreview[2].type = this.docType;
          this.imagePreview[2].uploadingF = true;
          this.imagePreview[2].approvedF = '';
          this.imagePreview[2].uploadedF = '1';
        }
      if (this.allowedExtensions.exec(fileUrl)) { 
        this.fileType = 'video';
        return false;
      } else {
        this.fileType = 'image';
        this.uploadImage();
       }
      }
      reader.readAsDataURL(event.target.files[0]);
      setTimeout(() => {
        this.imagePreview.filter((res , i) =>{
          if(res.uploadingB == true){
           res.uploadingB = false;
          } else if(res.uploadingF == true){
           res.uploadingF = false;
          }
        });
      }, 5000); 
    }
  }
  uploadImage(){
    let docModel = new UploadDocument(this.imageDocId , this.imagePreview[this.setImageDocTypeIfImageNotSet].type , 
      this.guideId ,'0', this.sydOfImage ,this.fileToUpload);
      console.log(docModel);
      let formData = new FormData();
      formData.append('doc_type',this.imagePreview[this.setImageDocTypeIfImageNotSet].type);
      formData.append('guide_id',this.guideId);
      formData.append('doc_id',this.imageDocId);
      formData.append('upload_type','0');
      formData.append('view_type',this.sydOfImage);
      formData.append('image',this.fileToUpload);
      console.log('my image form fata',formData.get('guide_id'));
      this.getDocs.uploadDocument(formData).subscribe(res => {
        console.log(res);
      });
  }
  readImageUrlBack(event , fileUrl ){
    this.activeProceedButton = false;
    this.sydOfImage = 'BACK';
    if (event.target.files && event.target.files[0]) {
      this.fileToUpload = event.target.files[0];

      this.allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
      const reader = new FileReader();
      reader.onload = (event:any) => {
        if(this.imageDocId == '1'){
          this.imagePreview[0].urlB = event.target.result;
          this.imagePreview[0].uploadingB = true;
          this.imagePreview[0].approvedB = '';
          this.imagePreview[0].uploadedB = '1';
        }
        if(this.imageDocId == '2'){
          this.imagePreview[1].urlB = event.target.result;
          this.imagePreview[1].uploadingB = true;
          this.imagePreview[1].approvedB = '';
          this.imagePreview[1].uploadedB = '1';
        }
        if(this.imageDocId == '3'){
          this.imagePreview[2].urlB = event.target.result;
          this.imagePreview[2].uploadingB = true;
          this.imagePreview[2].approvedB = '';
          this.imagePreview[2].uploadedB = '1';
        }
      // this.imageUrl = event.target.result;
      if (this.allowedExtensions.exec(fileUrl)) { 
        this.fileType = 'video';
        return false;
      } else {
        this.fileType = 'image';
        this.uploadImage();
       }
      }
      reader.readAsDataURL(event.target.files[0]);
    }
   setTimeout(() => {
     this.imagePreview.filter((res , i) =>{
       if(res.uploadingB == true){
        res.uploadingB = false;
       } else if(res.uploadingF == true){
        res.uploadingF = false;
       }
     });
   }, 5000); 
  }
  
  // image cropper methods
  imageChangedEvent: any = '';
  croppedImage: any = '';

  fileChangeEvent(event: any): void {
      this.imageChangedEvent = event;
  }
  imageCropped(image: string) {
      this.croppedImage = image;
  }
  imageLoaded() {
      // show cropper
  }
  loadImageFailed() {
      // show message
  }
  navigate(){
    this.commonHeader.activeLinks('true');
    localStorage.setItem('dataFilled','true');
    this.route.navigate(['']);
  }
  // applyActiveClass(i , z){
  //   if(i == 0 && this.imagePreview[z].type){
  //     return true;
  //   } else if(i==0 && this.imagePreview[z].uploaded == '0'){ 
  //     this.docType = "AADHAR CARD";
  //     return true;
  //   }
  // }
  activeProceed(){
    if(this.imagePreview[0].uploadedF == '0' || this.imagePreview[0].uploadedF == '' || 
    this.imagePreview[0].uploadedB == '0' || this.imagePreview[0].uploadedB == '' ||
    this.imagePreview[1].uploadedF == '0' ||  this.imagePreview[1].uploadedF =='' ||
    this.imagePreview[1].uploadedB == '0' || this.imagePreview[1].uploadedB == ''){
      return true;
    }else{
      return false;
    }
  }
}
