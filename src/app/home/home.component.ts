import { Component, OnInit } from '@angular/core';
import { CommonHeader } from '../commonHeader.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  checkGuideLogin;
  constructor(private commonHeader : CommonHeader) { }

  ngOnInit() {
    this.checkGuideLogin = localStorage.getItem('uname');
    if(this.checkGuideLogin!=null){
      this.commonHeader.changeHeaderColor(false);
    }else{
      this.commonHeader.changeHeaderColor(true);
    }
  }

}
