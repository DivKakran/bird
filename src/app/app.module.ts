import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';

import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import {GetOtp} from './services/getOtp.service';
import {RegistrationCompleteComponent} from './registration-complete/registration-complete.component';
import {GuideDashboardComponent} from './guide-dashboard/guide-dashboard.component';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {CommonHeader} from './commonHeader.service';
import {AuthGuard} from './auth-guard.service';

import {UploadMedia} from './services/media-upload.service';
import {Registration} from './services/registration.service';
import {GetDocumentList} from './upload-document/get-document-list.service';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { UploadDocumentComponent } from './upload-document/upload-document.component';
import { BankDetailComponent } from './bank-detail/bank-detail.component';
import {SelectModule} from 'ng-select';
import { CreatePackageComponent } from './create-package/create-package.component';
import { NgxEditorModule } from 'ngx-editor';
import { RatingAndEarningComponent } from './rating-and-earning/rating-and-earning.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { PendingTripsComponent } from './pending-trips/pending-trips.component';
import { ConvertDataTypeDirective } from './convert-data-type.directive';
import { UpcomingTripComponent } from './upcoming-trip/upcoming-trip.component';
import { PastTripsComponent } from './past-trips/past-trips.component';
import { CancelledTripsComponent } from './cancelled-trips/cancelled-trips.component';
import { ViewPassbookComponent } from './view-passbook/view-passbook.component';
import { TermsForTouristComponent } from './terms-for-tourist/terms-for-tourist.component';
import { CancellationPolicyComponent } from './cancellation-policy/cancellation-policy.component';
import { FaqComponent } from './faq/faq.component';
import { TermsForGuideComponent } from './terms-for-guide/terms-for-guide.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { NgDatepickerModule } from 'ng2-datepicker';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    RegistrationCompleteComponent,
    GuideDashboardComponent,
    UploadDocumentComponent,
    BankDetailComponent,
    CreatePackageComponent,
    RatingAndEarningComponent,
    PendingTripsComponent,
    ConvertDataTypeDirective,
    UpcomingTripComponent,
    PastTripsComponent,
    CancelledTripsComponent,
    ViewPassbookComponent,
    TermsForTouristComponent,
    CancellationPolicyComponent,
    FaqComponent,
    TermsForGuideComponent,
    PrivacyPolicyComponent,
  ],
  imports: [
    BrowserModule , FormsModule , HttpClientModule ,HttpModule ,BrowserAnimationsModule, AppRoutingModule,
    ToastrModule.forRoot({ positionClass: 'toast-top-center' , preventDuplicates: true}),  SelectModule,
    NgMultiSelectDropDownModule.forRoot() ,NgxEditorModule , ImageCropperModule  , NgDatepickerModule
  ],
  providers: [GetOtp , CommonHeader , UploadMedia , Registration , AuthGuard , GetDocumentList],
  bootstrap: [AppComponent]
})
export class AppModule { }
