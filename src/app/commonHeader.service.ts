import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
@Injectable()
export class CommonHeader{
    header = new Subject<boolean>();
    headerAfterLogin = new Subject();
    headerColor = new Subject<boolean>();
    updatedImageUrl = new Subject();
    activeButtonAfterFilledInfo = new Subject();
    applyHeaderClass(val){
        this.header.next(val);
    }
    chnageHeaderAfterLogin(guideDetails){
        this.headerAfterLogin.next(guideDetails);
    }
    changeHeaderColor(val){
        this.headerColor.next(val);
    }
    updateProfilePicture(updatedImage){
        this.updatedImageUrl.next(updatedImage);
    }
    activeLinks(val){
        this.activeButtonAfterFilledInfo.next(val);
    }
}