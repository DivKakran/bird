import { Component, OnInit } from '@angular/core';
import {Rating} from '../services/rating.service';
@Component({
  selector: 'app-rating-and-earning',
  templateUrl: './rating-and-earning.component.html',
  styleUrls: ['./rating-and-earning.component.css'],
  providers: [Rating]
})
export class RatingAndEarningComponent implements OnInit {
  ratingData:any;
  starRating:any;
  showPassbook = false;;
  constructor(private rating: Rating) { }

  ngOnInit() {
    this.rating.getRating().subscribe((res) => {
      if(res && res["body"]){
        this.ratingData = res["body"];
        this.starRating = new Array(+res["body"].star_ratings);
        console.log('rating data',res["body"]);
      }
    });
  }
  viewPassbook(){
    this.showPassbook = true;
  }
}
