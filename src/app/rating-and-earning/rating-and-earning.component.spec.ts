import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingAndEarningComponent } from './rating-and-earning.component';

describe('RatingAndEarningComponent', () => {
  let component: RatingAndEarningComponent;
  let fixture: ComponentFixture<RatingAndEarningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatingAndEarningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingAndEarningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
