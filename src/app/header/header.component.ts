import { Component, OnInit , HostListener} from '@angular/core';
import {GetOtp} from '../services/getOtp.service';
import {OtpModel} from './getOtpModel';
import {VerifyOtp} from './verifyOtpModel';
import {Router} from "@angular/router";

import { ToastrService } from 'ngx-toastr';
import {CommonHeader} from '../commonHeader.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  submitted = false;
  phoneNum = "";
  otp_id:any;
  otp_value="";
  openOtpForm = false;
  signUpOtpForm=false;
  onScrollApplyHeaderClass=false;
  userRegistrationPage=true;
  guideName = localStorage.getItem('uname');
  guideImage = localStorage.getItem('pic');
  changeHeaderColor = false;
  updatedGuideImage='';
  disableLink;
  constructor(private otp: GetOtp , private toastr: ToastrService , private router: Router , private commonHeader: CommonHeader) {
   }
 
  ngOnInit() {
    this.disableLink = localStorage.getItem('dataFilled');
    this.commonHeader.headerColor.subscribe(res =>{ 
      this.changeHeaderColor = res;
      this.userRegistrationPage = true;
    })
    this.commonHeader.header.subscribe(res => {
      this.userRegistrationPage = res;
    });
    this.commonHeader.headerColor.subscribe(res =>{
      this.changeHeaderColor = res;
    })
    this.commonHeader.headerAfterLogin.subscribe(res => {
      if(res){
        this.guideName = res["name"];
        this.guideImage = res["image_url"];
        localStorage.setItem("uname",res["name"]);
        localStorage.setItem("pic",res["image_url"]);
        localStorage.setItem('video',res["video_link_web"]);
        localStorage.setItem('docApproved',res["document_approved"]);
      }
    });
    this.commonHeader.updatedImageUrl.subscribe(res => {
      this.guideImage=<string>res;
    });
    this.commonHeader.activeButtonAfterFilledInfo.subscribe(res => {
      this.disableLink = true;
    });
  }
  showSuccess(message) {
    this.toastr.success(message);
  }
  showGuideStatus(guideStatus){
    if(guideStatus==0){
      this.toastr.warning("User doesn't exist");
    }
    else{
      this.toastr.warning('User Exist');
    }
  }
  showAlert(msg){
    this.toastr.error(msg);
  }
  getOtp(formValue){
    let otpModel = new OtpModel(formValue.mobile , '12345')
    this.otp.gettingOtp(otpModel).subscribe(response => {
      if(response && response["body"]){  
        this.otp_id = response["body"].otp_id;
        // closing mobile form
        this.signUpOtpForm=true;
        this.showSuccess(response["body"].message);
      }
    });
  }
  getOtp2(formValue){
    let otpModel = new OtpModel(formValue.mobile , '12345')
    this.otp.gettingOtp(otpModel).subscribe(response => {
      if(response && response["body"]){  
        // closing mobile form
        this.openOtpForm=true;
        this.otp_id = response["body"].otp_id;
        this.showSuccess(response["body"].message);
      }
    });
  }
  resendOtp(){
      let otpModel = new OtpModel(this.phoneNum , '12345')
      this.otp.gettingOtp(otpModel).subscribe(response => {
        if(response && response["body"]){  
          this.otp_id = response["body"].otp_id;
          this.showSuccess(response["body"].message);
        }
      });
  }
  verifyOtp(otp){
    if(otp && otp.otpValue!=''){
      let verifyOtp = new VerifyOtp(this.phoneNum , "12345" , this.otp_id ,otp.otpValue);
      this.otp.verifyOtp(verifyOtp).subscribe(response =>{
        if(response && response["body"]){
          if(response["body"].message!=""){
            this.showAlert("Otp didn't match");
            return false;
          }
          localStorage.setItem('guide_id',response["body"].guide_id);
          if(response["body"].has_registered==1 && response["body"].document_approved == 0) {
            localStorage.setItem('dataFilled', 'true');
            if(response["body"].guideDetails){ console.log('detail guide',response);
              this.commonHeader.chnageHeaderAfterLogin(response["body"].guideDetails);
              this.guideImage = response["body"].guideDetails.image_url;
            }
            document.getElementById("openModalButton").click();
            this.router.navigate(['registration/docs']); 
          }else if(response["body"].has_registered == 0){
            this.disableLink = 'false';
            localStorage.setItem('dataFilled', 'false');
            document.getElementById("openModalButton").click();
            this.router.navigate(['registration']); 
          }else if(response["body"].has_registered==1 && response["body"].document_approved == 1){
            localStorage.setItem('dataFilled', 'true');
            this.commonHeader.chnageHeaderAfterLogin(response["body"].guideDetails);
            this.guideImage = response["body"].guideDetails.image_url;
            document.getElementById("openModalButton").click();
            this.router.navigate(['registration/docs']); 
          }
          // this.showGuideStatus(response["body"].guide_exist);
        }
      })
    }
  }
  @HostListener("window:scroll", [])
   onWindowScroll() {
    let windowHeight = window.innerHeight;
    let position = document.documentElement.scrollTop + windowHeight;
    const scrollHeight = 700;
    const initialHeight = 800;
    if (position >= scrollHeight) {
      this.onScrollApplyHeaderClass = true;
    }
    else if (position <= initialHeight) {
      this.onScrollApplyHeaderClass = false;
    }
  }
  logOut(){
    // localStorage.removeItem('guide_id');
    // localStorage.removeItem('pic');
    // localStorage.removeItem('uname');
    // localStorage.removeItem('video');
    // localStorage.removeItem('dataFilled');
    localStorage.clear();
    this.openOtpForm = false;
    this.router.navigate(['']);
    window.location.reload(true);
  }
  navigate(val){
    if(val==undefined){
      this.router.navigate(['']);
    }else if(val=='registration'){
      this.router.navigate([val]);
     } else if(val=='dashboard'){
      this.router.navigate([val]);
     } else{
    this.router.navigate([val]);
    }
  }
  navigate2(val){
    if(val==undefined){
      this.router.navigate(['']);
    }else{ 
    this.router.navigate([val]);
    }
  }
}
