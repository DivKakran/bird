import { Component, OnInit } from '@angular/core';
import { PackageService } from '../services/package-service';
import {IOption} from 'ng-select';
import { ToastrService } from 'ngx-toastr';
import {CreatePackage} from './create-package-model';
@Component({
  selector: 'app-create-package',
  templateUrl: './create-package.component.html',
  styleUrls: ['./create-package.component.css'],
  providers:[PackageService]
})
export class CreatePackageComponent implements OnInit {
  cityListData=[];
  htmlContent=''
  ;
  selectedLanguages=[];
  dropdownSettings = {};
  availableDays=[];
  languages=[];
  selectedLanguageId=[];
  cityName='';
  place = '';
  getOffDays=['Mon ','Tue ','Wed ','Thu ','Fri ','Sat ','Sun '];
  myCities: Array<IOption> = [];
  placesInsideCities: Array<IOption> = [];
  createdPackageList=[];
  selectedLanguagesCreatedPackage=[];
  itinearyCreated=[];
  guideId = localStorage.getItem('guide_id');

  cityCreatedPackage=[];
  placeCreatedPackage=[];
  // prod error handling
  charges;
  duration;
  
  setClassOnDaysOf = {sun:false ,mon:false,tue:false,wed:false,thur:false,fri:false,sat:false};
  constructor(private packageService: PackageService ,  private toastr: ToastrService) { }

  onSelectCities(event){
    this.cityName = event.value;
    this.placesInsideCities=[];
    let cityName = event.label;
    this.cityListData.filter(res => { 
      if(res.name == cityName){
        res.placeList.filter(res => {
          let b = {label:'' , value:''};
          b.label = res.place_name;
          b.value = res.id;
          this.placesInsideCities.push(b);
        })
      }
    })
  }
  
  onLanguageSelect(event){
    this.selectedLanguageId.push(event.id);
    console.log(this.selectedLanguageId);
  }
  onLanguageDeSelect(event){
    this.selectedLanguageId.filter((res , i) =>{
      if(event.id == res){ 
        this.selectedLanguageId.splice(i,1);
      }
    }); console.log(this.selectedLanguageId);
  }
  check(){
    if(this.placesInsideCities.length<=0){ 
      this.toastr.warning('Select city first');
    }
  }
  onSelectedInternalPlace(event){
    this.setClassOnDaysOf = {sun:false ,mon:false,tue:false,wed:false,thur:false,fri:false,sat:false};
    let place = event.label;
    this.place = event.value;
    this.cityListData.filter(res => { 
        res.placeList.filter(res => {
        console.log(res);
        if(res.place_name == place){
          res.week_off.filter(res => {
            if(res == 'SUNDAY'){
              this.availableDays.filter((res , i) => {
                if(res == 'Sunday'){
                  delete this.availableDays[i];
                }
              })
              this.setClassOnDaysOf.sun=true;
            }
            else if(res == 'MONDAY'){
              this.availableDays.filter((res , i) => {
                if(res == 'Monday'){
                  delete this.availableDays[i];
                }
              })
              this.setClassOnDaysOf.mon=true;
            }
            else if(res == 'TUESDAY'){
              this.availableDays.filter((res , i)=> {
                if(res == 'Tuesday'){
                  delete this.availableDays[i];
                }
              })
              this.setClassOnDaysOf.tue = true;
            }
            else if(res == 'WEDNESDAY'){
              this.availableDays.filter((res, i) => {
                if(res == 'Friday'){
                  delete this.availableDays[i];
                }
              })
              this.setClassOnDaysOf.wed = true;
            }
            else if(res == 'THURSDAY'){
              this.availableDays.filter((res,i) => {
                if(res == 'Thursday'){
                  delete this.availableDays[i];
                }
              })
              this.setClassOnDaysOf.thur = true;
            }
            else if(res == 'FRIDAY'){
              this.availableDays.filter((res,i) => {
                if(res == 'Friday'){
                  delete this.availableDays[i];
                }
              })
              this.setClassOnDaysOf.fri = true;
            }
          })
        }
      })
    }); 
  }
  act={mon:false,tue:false,wed:false,thu:false,fri:false,sat:false,sun:false};
  selectDay(day){
    if(day == 'mon'){
      if(this.act.mon == false){
        this.act.mon=true;
        this.availableDays.push('Mon ');
        this.getOffDays.filter((res,i)=>{
          if(res == 'Mon '){
            delete this.getOffDays[i];
          }
        })
      }else{
        this.getOffDays.push('Mon ');
        this.availableDays.filter((res , i)=>{
          if(res=='Mon '){
            delete this.availableDays[i];
          }
        })
        this.act.mon = false;
      }
    }
    if(day == 'tue'){
      if(this.act.tue == false){
        this.act.tue=true;
        this.availableDays.push('Tue ');
        this.getOffDays.filter((res,i)=>{
          if(res == 'Tue '){
            this.getOffDays.splice(i,1);
          }
        })
      }else{
        this.getOffDays.push('Tue ');
        this.availableDays.filter((res , i)=>{
          if(res=='Tue '){
            delete this.availableDays[i];
          }
        })        
        this.act.tue = false;
      }
    }
    if(day == 'wed'){
      if(this.act.wed == false){
        this.availableDays.push('Wed ');
        this.act.wed = true;
        this.getOffDays.filter((res,i)=>{
          if(res == 'Wed '){
            this.getOffDays.splice(i,1);
          }
        })
      }else{
        this.getOffDays.push('Wed ');
        this.availableDays.filter((res , i)=>{
          if(res=='Wed '){
            delete this.availableDays[i];
          }
        })
        this.act.wed = false;
      }
    }
    if(day == 'thu'){
      if(this.act.thu == false){
        this.availableDays.push('Thu ');
        this.act.thu=true;
        this.getOffDays.filter((res,i)=>{
          if(res == 'Thu '){
            this.getOffDays.splice(i,1);
          }
        })
      }else{
        this.getOffDays.push('Thu ');
        this.availableDays.filter((res , i)=>{
          if(res=='Thu '){
            delete this.availableDays[i];
          }
        })
        this.act.thu = false;
      }
    }
    if(day == 'fri'){
      if(this.act.fri == false){
        this.availableDays.push('Fri ');
        this.act.fri=true;
        this.getOffDays.filter((res,i)=>{
          if(res == 'Fri '){
            this.getOffDays.splice(i,1);
          }
        })
      }else{
        this.getOffDays.push('Fri ');
        this.availableDays.filter((res , i)=>{
          if(res=='Fri '){
            delete this.availableDays[i];
          }
        })
        this.act.fri = false;
      }
    }
    if(day == 'sat'){
      if(this.act.sat == false){
        this.availableDays.push('Sat ');
        this.act.sat = true;
        this.getOffDays.filter((res,i)=>{
          if(res == 'Sat '){
            this.getOffDays.splice(i,1);
          }
        })
      }else{
        this.getOffDays.push('Sat ');
        this.availableDays.filter((res , i)=>{
          if(res=='Sat '){
            delete this.availableDays[i];
          }
        }) 
        this.act.sat = false;
      }
    }
    if(day == 'sun'){
      if(this.act.sun == false){
        this.availableDays.push('Sun ');
        this.act.sun=true;
        this.getOffDays.filter((res,i)=>{
          if(res == 'Sun '){
            this.getOffDays.splice(i,1);
          }
        })
      }else{
        this.getOffDays.push('Sun ');
        this.availableDays.filter((res , i)=>{
          if(res=='Sun '){
            delete this.availableDays[i];
          }
        })
        this.act.sun = false;
      }
    }
  }
  showWarnig(message){
    this.toastr.info(message);
  }
  showSuccess(message){
    this.toastr.success(message);
  }
  updatedCityList=[];
  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 4,
      allowSearchFilter: true
    };
    this.packageService.getPackageDetail().subscribe(res =>{
      if(res && res["body"]){
      this.languages = res["body"].languageList;
        this.cityListData = res["body"].cityList; 
        this.updatedCityList = res["body"].cityList; console.log('cityList',this.updatedCityList);
        if(res && res["body"].cityList.length>1){
          for(let i =0; i < res["body"].cityList.length; i++){
            let a = {label:'', value:''};
            a.label = res["body"].cityList[i].name;
            a.value = res["body"].cityList[i].id;
            this.myCities.push(a);
          }
        } this.gettingExistingPackageData('next');
      } 
    }); 
  }
  onSubmit(val){ 
    if(this.cityName == ''){
      this.toastr.warning('Select City First');
    }
    else if(this.place == ''){
      this.toastr.warning('Select Place');
    }
    else if(this.selectedLanguages.length<=0){
      this.toastr.warning('Select Language');
    }
    else if(!val.charges){
      this.toastr.warning('Add some charges');
    }
    else if(!val.duration){
      this.toastr.warning('Specify the duration');
    }
    else if(this.availableDays.length<=0){
      this.toastr.warning('Select Day');
    }else{
      document.getElementById('closeButton').click();
      let obj = new CreatePackage(this.guideId , this.cityName , this.place , this.selectedLanguageId,
       this.availableDays, val.charges , val.duration , this.htmlContent);
       this.packageService.createPackage(obj).then(res => { console.log('asda',obj);
         if(res["body"].status==1){
          //  document.getElementById('closeButton').click();
           this.gettingExistingPackageData('latest');
           this.toastr.success('Package Created Successfully');
         }
       })
    }
  }
 
  // update package start
  selectPlaceList=true;
  updatedMyPlaces=[];
  forUpdateThePackage=[];
  pageLoad = 0;
  totalPackages = 0;
  packageLoaded = 0;

  gettingExistingPackageData(val){
    if(val == 'next'){
      ++this.pageLoad;
      this.packageLoaded+=4;
    }else if(val == 'latest'){
      this.pageLoad = 1;
      this.packageLoaded = 4;
    }
    else{
      --this.pageLoad;
      this.packageLoaded-=4;
    }
    // for setting pagination initialise new array every time
    this.createdPackageList=[];
    this.forUpdateThePackage=[];
    // end initialisation
    this.packageService.getAllCreatedPackageOnPageLoad(this.pageLoad).subscribe(res =>{
      if(res && res["body"] && res["body"].packageList.length>0){
        this.totalPackages = res["body"].packageCount;
        this.createdPackageList = res["body"].packageList
        this.createdPackageList.filter((res,i)=>{
          let nullArray = [];
          this.updatedMyPlaces.push(nullArray);
          let obj = {city:res.city_id , durationHr:res.duration_hours ,
            itinearay:res.itinerary , language:res.language_ids ,
            place:res.place_id , weekOff:res.week_off , weekOn:res.week_on ,
            packageCharge:res.package_charged , 
            cDayOff:{sun:false ,mon:false,tue:false,wed:false,thu:false,fri:false,sat:false},
            cActiveDay:{sun:false ,mon:false,tue:false,wed:false,thu:false,fri:false,sat:false},
            cPAckageOffDay:[],cPackageAvailableDays:[]};
          this.forUpdateThePackage.push(obj);
        });console.log('forUpdateThePackage places',this.forUpdateThePackage);
        this.loadValueInExistPackage();
      }
    });
  }
  placeCreatedPackageSelect=[];
  loadValueInExistPackage(){
    this.createdPackageList.filter((res,i)=>{
      let obj = {label:res.city.city_name,value:res.city.id};
      this.cityCreatedPackage.push(obj);
      // set day off and on
      res.week_off.filter(r =>{
        if(r == "SUNDAY"){
          this.forUpdateThePackage[i].cDayOff.sun = true;
          // this.forUpdateThePackage[i].cPAckageOffDay.push('Sun');
        }else if(r == "MONDAY"){
          this.forUpdateThePackage[i].cDayOff.mon = true;
          // this.forUpdateThePackage[i].cPAckageOffDay.push('Mon');
        }else if(r == "TUESDAY"){
          this.forUpdateThePackage[i].cDayOff.tue = true;
          // this.forUpdateThePackage[i].cPAckageOffDay.push('Tue');
        }else if(r == "WEDNESDAY"){
          this.forUpdateThePackage[i].cDayOff.wed = true;
          this.forUpdateThePackage[i].cPAckageOffDay.push('Wed');
        }else if(r == "THURSDAY"){
          this.forUpdateThePackage[i].cDayOff.thu = true;
          // this.forUpdateThePackage[i].cPAckageOffDay.push('Thu');
        }else if(r == "FRIDAY"){
          this.forUpdateThePackage[i].cDayOff.fri = true;
          // this.forUpdateThePackage[i].cPAckageOffDay.push('Fri');
        }else if(r == "SATURDAY"){
          this.forUpdateThePackage[i].cDayOff.sat = true;
          // this.forUpdateThePackage[i].cPAckageOffDay.push('Sat');
        }
      });
      res.week_on.filter(r =>{
        if(r == "Sun "){
          this.forUpdateThePackage[i].cPackageAvailableDays.push('Sun');
          this.forUpdateThePackage[i].cActiveDay.sun = true;
        }else if(r == "Mon "){
          this.forUpdateThePackage[i].cPackageAvailableDays.push('Mon');
          this.forUpdateThePackage[i].cActiveDay.mon = true;
        }else if(r == "Tue "){
          this.forUpdateThePackage[i].cPackageAvailableDays.push('Tue');
          this.forUpdateThePackage[i].cActiveDay.tue = true;
        }else if(r == "Wed "){
          this.forUpdateThePackage[i].cPackageAvailableDays.push('Wed');
          this.forUpdateThePackage[i].cActiveDay.wed = true;
        }else if(r == "Thu "){
          this.forUpdateThePackage[i].cPackageAvailableDays.push('Thu');
          this.forUpdateThePackage[i].cActiveDay.thu = true;
        }else if(r == "Fri "){
          this.forUpdateThePackage[i].cPackageAvailableDays.push('Fri');
          this.forUpdateThePackage[i].cActiveDay.fri = true;
        }else if(r == "Sat "){
          this.forUpdateThePackage[i].cPackageAvailableDays.push('Sat');
          this.forUpdateThePackage[i].cActiveDay.sat = true;
        }
      }); 
      // add unavailable day
      for(let key in this.forUpdateThePackage[i].cActiveDay){;
        if(this.forUpdateThePackage[i].cActiveDay[key] == false){
          this.forUpdateThePackage[i].cPAckageOffDay.push(key);
        }
      }
    });console.log('-----------------',this.forUpdateThePackage)
    // show selected place
    this.createdPackageList.filter((res,i) =>{
      let obj = {label:res.place.place_name,value:res.place_id};
      this.placeCreatedPackageSelect.push(obj);
    });
    // filling places corresponding to selected City 
    if(this.updatedCityList){ 
    this.createdPackageList.filter((res,i) =>{ 
      this.updatedCityList.filter((r)=>{
        if(res.city.city_name == r.name){
          r.placeList.filter((res) => {
            let b = {label:'' , value:''};
            b.label = res.place_name;
            b.value = res.id; 
            this.updatedMyPlaces[i].push(b); 
          });
        } 
      });
    }); }console.log('created packGE',this.createdPackageList);
    this.createdPackageList.filter((res,i) =>{
      this.itinearyCreated.push(res.itinerary);
      if(res.language){
        this.selectedLanguagesCreatedPackage.push(res.language);
      }console.log('selected langugaes',this.selectedLanguagesCreatedPackage);
    });
    setTimeout(function(){
      document.getElementById("foo").click(); 
    }, 50);
  }
  
  onSelectCitiesCreatedPackages(event,index){
    this.updatedMyPlaces[index]=[];
      this.updatedCityList.filter((r)=>{ 
        if(event == r.name){
          // change value to update the package
          this.forUpdateThePackage[index].city = r.id;
          r.placeList.filter((res) => {
            let b = {label:'' , value:''};
            b.label = res.place_name;
            b.value = res.id; 
            this.updatedMyPlaces[index].push(b); 
          });
        }else if(event == 'Select'){
          this.forUpdateThePackage[index].city = event;
        }
    });  
  }
  // on change the place 
  onSelectedInternalPlaceCreatedPackage(event,index){ 
    this.forUpdateThePackage[index].place = event;
  }
  // language select
  selectLangCreatedPacakeges(event, index){
    this.forUpdateThePackage[index].language.push(event.id);
  }
  // language deslect
  deSelectLangCreatedPackages(event , index){
    this.forUpdateThePackage[index].language.filter((res,i)=>{
      if(res == event.id){
        this.forUpdateThePackage[index].language.splice(i,1);
      }
    });
  }
  checkUpdate(){
    if(this.placeCreatedPackage.length<=0){ 
      this.toastr.warning('Select city first');
    }
  }
  // for days selection
  cPackageSelectDay(day,i){ 
    if(day=='mon'){
      if(this.forUpdateThePackage[i].cActiveDay.mon == true){
        this.forUpdateThePackage[i].cActiveDay.mon = false;
        // set available days
        this.forUpdateThePackage[i].cPackageAvailableDays.filter((res,j)=>{
          if(res=="Mon"){
            this.forUpdateThePackage[i].cPackageAvailableDays.splice(j,1);
          }
        });
        // set unavailable day
        this.forUpdateThePackage[i].cPAckageOffDay.push("mon");
      } else{
        this.forUpdateThePackage[i].cActiveDay.mon = true;
        this.forUpdateThePackage[i].cPackageAvailableDays.push('Mon');
        // ṣet unavailable day
        this.forUpdateThePackage[i].cPAckageOffDay.filter((res,j) =>{
          if(res == "mon"){
            this.forUpdateThePackage[i].cPAckageOffDay.splice(j,1);
          }
        });
      }
    }else if(day=="tue"){
      if(this.forUpdateThePackage[i].cActiveDay.tue == true){
        this.forUpdateThePackage[i].cActiveDay.tue = false;
        // set available days
        this.forUpdateThePackage[i].cPackageAvailableDays.filter((res,j)=>{
          if(res=="Tue"){
            this.forUpdateThePackage[i].cPackageAvailableDays.splice(j,1);
          }
        });
        // set unavailable day
        this.forUpdateThePackage[i].cPAckageOffDay.push("tue");
      }else{
        this.forUpdateThePackage[i].cActiveDay.tue = true;
        this.forUpdateThePackage[i].cPackageAvailableDays.push('Tue');
        // ṣet unavailable day
        this.forUpdateThePackage[i].cPAckageOffDay.filter((res,j) =>{
          if(res == "tue"){
            this.forUpdateThePackage[i].cPAckageOffDay.splice(j,1);
          }
        });
      }
    }else if(day == "wed"){
      if(this.forUpdateThePackage[i].cActiveDay.wed == true){
        this.forUpdateThePackage[i].cActiveDay.wed = false;
         // set available days
         this.forUpdateThePackage[i].cPackageAvailableDays.filter((res,j)=>{
          if(res=="Wed"){
            this.forUpdateThePackage[i].cPackageAvailableDays.splice(j,1);
          }
        });
        // set unavailable day
        this.forUpdateThePackage[i].cPAckageOffDay.push("wed");
      }else{
        this.forUpdateThePackage[i].cActiveDay.wed = true;
        this.forUpdateThePackage[i].cPackageAvailableDays.push('Wed');
        // ṣet unavailable day
        this.forUpdateThePackage[i].cPAckageOffDay.filter((res,j) =>{
          if(res == "wed"){
            this.forUpdateThePackage[i].cPAckageOffDay.splice(j,1);
          }
        });
      }
    }else if(day == "thu"){
      if(this.forUpdateThePackage[i].cActiveDay.thu == true){
        this.forUpdateThePackage[i].cActiveDay.thu = false;
         // set available days
        this.forUpdateThePackage[i].cPackageAvailableDays.filter((res,j)=>{
          if(res=="Thu"){
            this.forUpdateThePackage[i].cPackageAvailableDays.splice(j,1);
          }
        });
        // set unavailable day
        this.forUpdateThePackage[i].cPAckageOffDay.push("thu");
      }else{
        this.forUpdateThePackage[i].cActiveDay.thu = true;
        this.forUpdateThePackage[i].cPackageAvailableDays.push('Thu');
        // ṣet unavailable day
        this.forUpdateThePackage[i].cPAckageOffDay.filter((res,j) =>{
          if(res == "thu"){
            this.forUpdateThePackage[i].cPAckageOffDay.splice(j,1);
          }
        });
      }
    }else if(day == "fri"){
      if(this.forUpdateThePackage[i].cActiveDay.fri == true){
        this.forUpdateThePackage[i].cActiveDay.fri = false;
        this.forUpdateThePackage[i].cPackageAvailableDays.filter((res,j)=>{
          if(res=="Fri"){
            this.forUpdateThePackage[i].cPackageAvailableDays.splice(j,1);
          }
        });
         // set unavailable day
         this.forUpdateThePackage[i].cPAckageOffDay.push("fri");
      } else{
        this.forUpdateThePackage[i].cActiveDay.fri = true;
        this.forUpdateThePackage[i].cPackageAvailableDays.push('Fri');
         // ṣet unavailable day
         this.forUpdateThePackage[i].cPAckageOffDay.filter((res,j) =>{
          if(res == "fri"){
            this.forUpdateThePackage[i].cPAckageOffDay.splice(j,1);
          }
        });
      }
    }else if(day == "sat"){
      if(this.forUpdateThePackage[i].cActiveDay.sat == true){
        this.forUpdateThePackage[i].cActiveDay.sat = false;
         // set available days
        this.forUpdateThePackage[i].cPackageAvailableDays.filter((res,j)=>{
          if(res=="Sat"){
            this.forUpdateThePackage[i].cPackageAvailableDays.splice(j,1);
          }
        });
         // set unavailable day
         this.forUpdateThePackage[i].cPAckageOffDay.push("sat");
      }else{
        this.forUpdateThePackage[i].cActiveDay.sat = true;
        this.forUpdateThePackage[i].cPackageAvailableDays.push('Sat');
        // ṣet unavailable day
        this.forUpdateThePackage[i].cPAckageOffDay.filter((res,j) =>{
          if(res == "sat"){
            this.forUpdateThePackage[i].cPAckageOffDay.splice(j,1);
          }
        });
      }
    }else if(day == "sun"){
      if(this.forUpdateThePackage[i].cActiveDay.sun == true){
        this.forUpdateThePackage[i].cActiveDay.sun = false;
         // set available days
        this.forUpdateThePackage[i].cPackageAvailableDays.filter((res,j)=>{
          if(res=="Sun"){
            this.forUpdateThePackage[i].cPackageAvailableDays.splice(j,1);
          }
        });
        // set unavailable day
        this.forUpdateThePackage[i].cPAckageOffDay.push("sun");
      }else{
        this.forUpdateThePackage[i].cActiveDay.sun = true;
        this.forUpdateThePackage[i].cPackageAvailableDays.push('Sun');
        // ṣet unavailable day
        this.forUpdateThePackage[i].cPAckageOffDay.filter((res,j) =>{
          if(res == "sun"){
            this.forUpdateThePackage[i].cPAckageOffDay.splice(j,1);
          }
        });
      }
    }
  }
  // end day selection method
  checkValueFieldValue(val,days){
    if(val.city=='Select'){
      this.toastr.warning("Select city"); 
      return false;
    } else if(val.place == 'Select'){
      this.toastr.warning('Select Place');
      return false;
    } else if(val.language.length<1){
      this.toastr.warning('Select a language');
      return false;
    } else if(val.packageCharge == ''){
      this.toastr.warning('Enter package charge');
      return false;
    } else if(val.durationHr == ''){
      this.toastr.warning('Fill duration');
      return false;
    } else if(days.length<1){
      this.toastr.warning('Select avaiable days');
      return false;
    } else{
      return true;
    }
  }
  updatePackage(pId,index){
    let dayAvailable = [];
    this.forUpdateThePackage[index].cPackageAvailableDays.filter((res,i) =>{
      if(res == 'Sun'){ 
        dayAvailable.push('Sun ');
      }else if(res=='Mon'){
        dayAvailable.push('Mon ');
      }else if(res == 'Tue'){
        dayAvailable.push('Tue ');
      }else if(res == 'Wed'){
        dayAvailable.push('Wed ');
      }else if(res == 'Thu'){
        dayAvailable.push('Thu ');
      }else if(res == 'Fri'){
        dayAvailable.push('Fri ');
      }else if(res == 'Sat'){
        dayAvailable.push('Sat ');
      }
    });
    if(this.checkValueFieldValue(this.forUpdateThePackage[index],dayAvailable)){
      let obj = new CreatePackage(this.guideId , this.forUpdateThePackage[index].city , 
        this.forUpdateThePackage[index].place , this.forUpdateThePackage[index].language,
        dayAvailable , this.forUpdateThePackage[index].packageCharge ,
        this.forUpdateThePackage[index].durationHr , this.forUpdateThePackage[index].itinearay,pId);

      this.packageService.createPackage(obj).then(res => { console.log('asda',obj);
        if(res["body"].status==1){ console.log('Update hogya',res);
          this.toastr.success('Package Updated Successfully');
        }
      })
    }
  }
  onSelectAll(event){
    console.log('checked');
  }
  deletePackage(pId){
    this.packageService.deletePackage(pId).subscribe((res) => { console.log('after result',res);
      if(res && res['body'] && res['body'].status ==1){
        this.pageLoad = 0; 
        this.packageLoaded = 0;
        this.gettingExistingPackageData('next');
        this.toastr.success('Package Deleted Successfully');
      }
    });
  }
}
