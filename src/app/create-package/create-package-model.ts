export class CreatePackage{
    guide_id:any;
    city_id:any;
    id:any;
    language_ids = [];
    week_on = [];
    package_charges:any;
    duration_hours:any;
    itinerary:any;
    package_id:any;
    constructor(guideId , cityId , id , languageId , weekOn , packageCharges , durationHours ,
    itinerary , packageId?:any){
        this.guide_id = guideId;
        this.city_id = cityId;
        this.id = id;
        this.language_ids = languageId;
        this.week_on = weekOn;
        this.package_charges = packageCharges;
        this.duration_hours = durationHours;
        this.itinerary = itinerary;
        this.package_id = packageId;
    }
}