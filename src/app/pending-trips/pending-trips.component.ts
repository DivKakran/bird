import { Component, OnInit } from '@angular/core';
import { DashboardServices } from '../services/dashboard-service';

@Component({
  selector: 'app-pending-trips',
  templateUrl: './pending-trips.component.html',
  styleUrls: ['./pending-trips.component.css'] ,
  providers: [DashboardServices]
})
export class PendingTripsComponent implements OnInit {
  noTrip = false;
  tripDetails:any;
  tripsDetailPop:any;
  requestStatus = {pending:0 , accept:1 , reject:2 , timeOut: 3};
  constructor(private dashService: DashboardServices) { }

  ngOnInit() {
    this.getTourDetailOnLoad();
  }
  getTourDetailOnLoad(){
    this.dashService.getTrips('REQUESTS').subscribe((res) =>{
      if(res && res['body']){
        if(res['body'].tour == null){
          this.noTrip = true; 
        }else if(res['body'].tour.length==0){
          this.noTrip = true; 
        } else{
          this.tripDetails = res['body'].tour;console.log('request tripe', res['body'].tour);
          this.tripDetails.filter((res , i) =>{
            this.tripDetails[i].time_remaining = parseInt(res.time_remaining)/3600;
            this.tripDetails[i].hideModal = true;
            if(res.tour_name.length>22){
              res.tour_name = res.tour_name.substr(0,20) + '…'; 
            }
          });
        }
      }
    });
  }
  numOfPersons:any;
  getTourDetail(id , tripType , bId , item){
    this.tripDetails.filter((r , i) =>{
      if(r.id != id){
        r.hideModal = false;
      }else{
        r.hideModal = true;
      }
    });
    this.dashService.getTripDetail(id , tripType , bId).subscribe((res) => {
      if(res && res['body']){
        this.tripsDetailPop = res['body'].tripDetails; 
        this.numOfPersons = parseInt(this.tripsDetailPop.no_of_adults) + 
        parseInt(this.tripsDetailPop.no_of_children);
      }
    });
  }
  closeViewDetail(){
    document.getElementById('close').click();
  }
  acceptRequest(id , user_id){
    document.getElementById('close1').click();
    this.dashService.acceptRequest(id , user_id).subscribe((res) =>{
      console.log('Accept Request' , res);
    });
  }
  rejectRequest(id , user_id){
    document.getElementById('close2').click();
    this.dashService.rejectRequest(id , user_id).subscribe((res) => {
      console.log('Reject Request' , res);
    });
  }
  cancelRequest(id , user_id){
    document.getElementById('close3').click();
    this.dashService.cancelTrip(id , user_id).subscribe((res) => {
      console.log('cancel Request' , res);
      this.getTourDetailOnLoad();
    });
  }
}
