import { NgModule } from '@angular/core';
import {Routes , RouterModule } from '@angular/router';

// import component
import {GuideDashboardComponent} from '../guide-dashboard/guide-dashboard.component';
import {RegistrationCompleteComponent} from '../registration-complete/registration-complete.component';
import {HomeComponent} from '../home/home.component';
import {AuthGuard} from '../auth-guard.service';
import {TermsForTouristComponent} from '../terms-for-tourist/terms-for-tourist.component';
import {TermsForGuideComponent} from '../terms-for-guide/terms-for-guide.component';
import {CancellationPolicyComponent} from '../cancellation-policy/cancellation-policy.component';
import {PrivacyPolicyComponent} from '../privacy-policy/privacy-policy.component';
import {FaqComponent} from '../faq/faq.component';

const routes: Routes = [
  { path:'', component:HomeComponent},
  { path:'registration', component:RegistrationCompleteComponent , canActivate:[AuthGuard]},
  { path:'dashboard', component:GuideDashboardComponent },
  { path:'registration/docs', component:RegistrationCompleteComponent,canActivate:[AuthGuard] },
  { path:'registration/create-package' , component: RegistrationCompleteComponent },
  { path:'registration/bank' , component: RegistrationCompleteComponent},
  { path:'registration/rating-and-earning' , component: RegistrationCompleteComponent},
  { path:'registration/my-profile' , component: RegistrationCompleteComponent},
  { path:'registration/my-places' , component: RegistrationCompleteComponent},
  { path:'terms-for-tourist' , component: TermsForTouristComponent},
  { path:'terms-for-guide' , component: TermsForGuideComponent},
  { path:'cancellation-policy' , component:CancellationPolicyComponent},
  { path:'privacy-policy' , component:PrivacyPolicyComponent},
  { path:'faq' , component:FaqComponent},
  {path: '**' , redirectTo: ''}
]

@NgModule({
  imports: [RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
